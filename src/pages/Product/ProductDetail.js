import React, { Component } from "react";
import "../../constants/css/Product.css";
import ProductImg from "../../components/Product/ProductImg";
import { Button, message } from "antd";
import { connect } from "react-redux";
import { openAuthModal, closeAuthModal } from "../../redux/action/Auth";
import axios from "axios";
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      productData: [],
      filteredData: []
    };
  }
  payItem = () => {
    if (this.state.userData !== null) {
      message.info("hi");
    } else {
      message
        .error("Please Login First")
        .then(this.props.dispatch(openAuthModal()));
    }
  };

  filterData = category => {
    let newData = this.state.productData.filter(data => {
      return data.category === category;
    });

    this.setState({ filteredData: newData });
  };
  componentDidMount() {
    const { match } = this.props;
    const uid = match.params.uid;
    axios
      .get(`http://localhost:3001/product/${uid}`)
      .then(response => {
        this.setState({ productData: response.data });
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  }

  render() {
    let filteredData = this.state.filteredData.map(item => {
      return (
        <div class="productDetailDesc">
          <h2 class="productDetailHeader">
            RM{item.price} {item.service}
          </h2>
          <Button >Chat Now</Button>
          <Button onClick={this.payItem}>
            Buy Now
          </Button>
        </div>
      );
    });

    let ProductsHeader = this.state.productData.map(item => {
      return (
        <div
          onClick={() => {
            this.filterData(item.category);
          }}
        >
          <p class="productDetailHeader">{item.category}</p>
        </div>
      );
    });

    return (
      <div class="productDetail">
        <ProductImg />
        <div class="productDetailContent">
          <div class="productGridHeader">{ProductsHeader}</div>
          <div>{filteredData}</div>
        </div>
      </div>
    );
  }
}

export default connect()(ProductDetail);
