import React, { Component } from "react";
import "../../constants/css/Profile.css";
import ProfileNav from "../../components/Profile/ProfileNav";

// Tabs Items
let profileTabs = ["Order", "Product", "Personal"];

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "Order"
    };
  }

  render() {
    let tabs = profileTabs.map(item => {
      return (
        <div
          class="profileTabsMenu"
          onClick={() => {
            this.setState({ content: item });
          }}
        >
          <h1>{item}</h1>
        </div>
      );
    });

    return (
      <div class="profile">
        <div class="profileTabs">{tabs}</div>}
        <div class="profileContent">
          <ProfileNav content={this.state.content} />
        </div>
      </div>
    );
  }
}

export default Profile;
