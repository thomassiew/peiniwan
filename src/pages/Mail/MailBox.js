import React, { Component } from "react";
import "../../constants/css/Mail.css";
import { Input, Button } from "antd";
import axios from "axios";
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      focusedMessageRoom: null,
      typedMessage: "",
      messageRoom: [],
      messages: []
    };
  }
  updateMessage = e => {
    this.setState({ typedMessage: e.target.value });
  };
  async getMessages(id) {
    try {
      axios
        .get(`http://localhost:3001/mailroom/${id}/messages`)
        .then(response => {
          this.setState({ messages: response.data });
          this.setState({ focusedMessageRoom: id });
          console.log("yeap");
        })
        .catch(function(error) {
          console.log(`Error: ${error}`);
        });
    } catch (e) {
      console.log(e);
    }
  }

  sendMessage = () => {
    axios
      .post("http://localhost:3001/mail/sendmessage", {
        senderId: this.state.userData.uid,
        message: this.state.typedMessage,
        mailRoomId: this.state.focusedMessageRoom
      })
      .then(response => {
        this.getMessages(this.state.focusedMessageRoom);
      });

  };
  componentDidMount() {
    axios
      .get(`http://localhost:3001/mailroom/${this.state.userData.uid}`)
      .then(response => {
        this.setState({ messageRoom: response.data });

        setInterval(this.getMessages(this.state.focusedMessageRoom), 3000);
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  }
  render() {
    let MessageRoom = this.state.messageRoom.map(id => {
      return (
        <div
          class="mailCard"
          onClick={() => {
            this.getMessages(id);
          }}
        >
          <h2>{id}</h2>
        </div>
      );
    });

    let contents = this.state.messages.map(messaging => {
      return (
        <div class="mailMessages">
          <h3>{messaging.message}</h3>
          <h3>{messaging.created}</h3>
        </div>
      );
    });
    return (
      <div class="mail">
        <div class="mailPerson">{MessageRoom}</div>
        <div class="mailContent">
          <div class="mailMessageContainer">{contents}</div>
          <div class="mailButtons">
            <Input
              placeholder="Type your message"
              style={{ width: "90%" }}
              onChange={this.updateMessage}
            />
            <Button type="primary large" onClick={this.sendMessage}>
              Send
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default Product;
