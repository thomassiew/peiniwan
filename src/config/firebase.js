import firebase from "firebase";
import { message } from "antd";
import axios from "axios";

const config = {
  apiKey: "AIzaSyBxycf0oHoADlb4qC73fMnwBUMxrbBEATg",
  authDomain: "peiwanapp-e6d20.firebaseapp.com",
  databaseURL: "https://peiwanapp-e6d20.firebaseio.com",
  projectId: "peiwanapp-e6d20",
  storageBucket: "peiwanapp-e6d20.appspot.com",
  messagingSenderId: "632921573999"
};

let firebaseConfig = firebase.initializeApp(config);
let googleProvider = new firebase.auth.GoogleAuthProvider();
let facebookProvider = new firebase.auth.FacebookAuthProvider();

export const signInWithFacebook = () => {
  firebaseConfig
    .auth()
    .signInWithPopup(facebookProvider)
    .then(result => {
      axios.post("http://localhost:3001/user/auth", {
        name: result.user.displayName,
        email: result.user.email,
        firebaseUid: result.user.uid
      });
      localStorage.setItem("user", JSON.stringify(result.user));
      window.location.reload();
    })
    .catch(error => {
      console.log(error);
      message.error(error.message);
    });
};
export const signInWithGoogle = () => {
  firebaseConfig
    .auth()
    .signInWithPopup(googleProvider)
    .then(result => {
      axios.post("http://localhost:3001/user/auth", {
        name: result.user.displayName,
        email: result.user.email,
        firebaseUid: result.user.uid
      });
      localStorage.setItem("user", JSON.stringify(result.user));
      window.location.reload();
    })
    .catch(error => {
      console.log(error);
      message.error(error.message);
    });
};

export const signOut = () => {
  firebaseConfig
    .auth()
    .signOut()
    .then(function() {
      localStorage.removeItem("user");
      window.location.reload();
    })
    .catch(function(error) {
      console.log(error);
      message.error(error.message);
    });
};
