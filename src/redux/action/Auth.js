import ActionTypes from "../../constants/ActionTypes";

export const openAuthModal = () => {
  console.log("open Auth Modal");
  return {
    type: ActionTypes.OPEN_AUTH_MODAL
  };
};

export const closeAuthModal = () => {
  console.log("close Auth Modal");
  return {
    type: ActionTypes.CLOSE_AUTH_MODAL
  };
};