import ActionTypes from "../../constants/ActionTypes";

const initialState = {
  isOpenModal: false
};

const authModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.CLOSE_AUTH_MODAL:
      return {
        ...state,
        isOpenModal: false
      };
    case ActionTypes.OPEN_AUTH_MODAL:
      return {
        ...state,
        isOpenModal: true
      };
    default:
      return state;
  }
};
export default authModalReducer;
