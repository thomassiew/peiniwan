import { createStore, combineReducers } from "redux";
import authModalReducer from "../reducer/authReducer";

const rootReducer = combineReducers({
  auth: authModalReducer 
});

const store = () => {
  return createStore(rootReducer);
};

export default store;
