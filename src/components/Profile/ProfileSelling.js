import React, { Component } from "react";
import { Button, Modal } from "antd";
import ProfileSellingDetails from "./ProfileSellingDetails";
import axios from "axios";
import GameCategories from "../../constants/GameCategories";

let columns = ["Game", "Price", "Service Mode"];

class ProfileSelling extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      showModal: false,
      productData: []
    };
    console.log(GameCategories);
  }

  componentDidMount() {
    axios
      .get(`http://localhost:3001/product/${this.state.userData.uid}`)
      .then(response => {
        this.setState({ productData: response.data });
        console.log(this.state.productData);
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  }

  showModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    let Products = this.state.productData.map(item => {
      return (
        <div class="profileProductRows">
          <h2 class="profileProductRowsContent">{item.category}</h2>
          <h2 class="profileProductRowsContent">{item.price}</h2>
          <h2 class="profileProductRowsContent">{item.service}</h2>
        </div>
      );
    });

    return (
      <div>
        <div class="profileGenericButton">
          <Button onClick={this.showModal} size="large">
            Add Product
          </Button>
        </div>
        <div class="profileProductRows">
          <h1 class="profileProductRowsContent">{columns[0]}</h1>
          <h1 class="profileProductRowsContent">{columns[1]}</h1>
          <h1 class="profileProductRowsContent">{columns[2]}</h1>
        </div>
        <div>{Products}</div>
        <Modal
          title="Add Product"
          visible={this.state.showModal}
          footer={null}
          onCancel={this.closeModal}
          width={420}
        >
          <ProfileSellingDetails />
        </Modal>
      </div>
    );
  }
}

export default ProfileSelling;
