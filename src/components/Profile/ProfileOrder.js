import React, { Component } from "react";

let columns = ["Order Id", "Product", "Price", "Status"];

class ProfileOrder extends Component {
  
  render() {
    let OrderColumn = columns.map(item => {
      return (
        <div class="profileColumn">
          <h2>{item}</h2>
        </div>
      );
    });

    return <div class="profileGeneric">{OrderColumn}</div>;
  }
}

export default ProfileOrder;
