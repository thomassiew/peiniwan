import React, { Component } from "react";
import axios from "axios";
import { DatePicker, Input, Button } from "antd";
import moment from "moment";

const dateFormat = "YYYY-MM-DD";

class ProfilePersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      name: null,
      birthday: new Date()
    };
  }
  componentDidMount() {
    axios
      .get(`http://localhost:3001/user/${this.state.userData.uid}`)
      .then(response => {
        this.setState({ name: response.data.name.toString() });
        if (response.data.birthday != null) {
          this.setState({
            birthday: new Date(response.data.birthday)
          });
        }
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  }

  updateName = e => {
    this.setState({ name: e.target.value });
  };

  updateBday = e => {
    console.log(e.format());
    this.setState({ birthday: e.format() });
  };

  savePersonal = () => {
    axios
      .put(`http://localhost:3001/user/update/${this.state.userData.uid}`, {
        name: this.state.name,
        birthday: this.state.birthday
      })
      .then(function(response) {
        console.log(response);
        window.location.reload();
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  };
  render() {
    return (
      <div class="PersonalDetails">
        <div class="personalDetailsContent">
          <p> Name </p>
          <Input
            placeholder="Name"
            style={{ width: "50%" }}
            value={this.state.name}
            onChange={this.updateName}
          />
        </div>
        <div class="personalDetailsContent">
          <p> Birthday </p>
          <DatePicker
            onChange={this.updateBday}
            value={moment(this.state.birthday, dateFormat)}
            format={dateFormat}
            style={{ width: "50%" }}
          />
        </div>

        <div
          class="personalDetailsContent"
          style={{ marginTop: "50px" }}
          onClick={this.savePersonal}
        >
          <Button type="primary">Save</Button>
        </div>
      </div>
    );
  }
}

export default ProfilePersonal;
