import React, { Component } from "react";
import { Select, Button,message } from "antd";
import GameCategories from "../../constants/GameCategories";
import Pricing from "../../constants/Pricing";
import ServiceMode from "../../constants/ServiceMode";
import axios from "axios";
let Option = Select.Option;

class ProfileSellingDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      showModal: false,
      category: "",
      price: "",
      service: ""
    };
  }

  addProduct = () => {
    axios
      .post("http://localhost:3001/product/add", {
        category: this.state.category,
        price: this.state.price,
        service: this.state.service,
        firebaseUid: this.state.userData.uid
      })
      .then(response => {
        window.location.reload()
        message.success(`Successfully saved`);
      })
      .catch(error => {
        console.log(error);
        message.error(error.message);
      });
  };

  addCategory = e => {
    this.setState({ category: e });
  };

  addPrice = e => {
    this.setState({ price: e });
  };

  addService = e => {
    this.setState({ service: e });
  };
  render() {
    let gameCategories = GameCategories.map(item => {
      return <Option value={item.Game}>{item.Game}</Option>;
    });

    let priceValue = Pricing.map(item => {
      return <Option value={item.Price}>RM{item.Price}</Option>;
    });

    let serviceMode = ServiceMode.map(item => {
      return <Option value={item.Service}>{item.Service}</Option>;
    });
    return (
      <div style={{ display: "flex", flex: 1, flexDirection: "column" }}>
        <div class="SellingDetails">
          <p> Game Category</p>
          <Select
            defaultValue=""
            style={{ width: 160 }}
            onChange={this.addCategory}
          >
            {gameCategories}
          </Select>
        </div>
        <div class="SellingDetails">
          <p>Price</p>
          <Select
            defaultValue=""
            style={{ width: 160 }}
            onChange={this.addPrice}
          >
            {priceValue}
          </Select>
        </div>
        <div class="SellingDetails">
          <p>Service Mode</p>
          <Select
            defaultValue=""
            style={{ width: 160 }}
            onChange={this.addService}
          >
            {serviceMode}
          </Select>
        </div>
        <div class="SellingDetails" style={{ marginTop: "50px" }}>
          <Button block type="primary" onClick={this.addProduct}>
            Save
          </Button>
        </div>
      </div>
    );
  }
}

export default ProfileSellingDetails;
