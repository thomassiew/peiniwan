import React, { Component } from "react";
import "../../constants/css/Profile.css";

import ProfileOrder from "./ProfileOrder";
import ProfilePersonal from "./ProfilePersonal";
import ProfileSelling from "./ProfileSelling";

class ProfileNav extends Component {
  render() {
    const { content } = this.props;

    const profileContent = content => {
      if (content === "Order") {
        return <ProfileOrder />;
      } else if (content === "Product") {
        return <ProfileSelling />;
      } else {
        return <ProfilePersonal />;
      }
    };

    return <div>{profileContent(content)}</div>;
  }
}

export default ProfileNav;
