import React, { Component } from "react";
import "../../constants/css/Product.css";
import { message } from "antd";
import GameCategories from "../../constants/GameCategories";
import { Link } from "react-router-dom";
import axios from "axios";
class ProductGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null || JSON.parse(localStorage.getItem("user")),
      productData: []
    };
  }
  selectCategory = category => {
    console.log(category);
    axios
      .get(`http://localhost:3001/product/category/${category}`)
      .then(response => {
        console.log(response.data);
        if (response.data.length === 0) {
          axios
            .get(`http://localhost:3001/product/category/all`)
            .then(response => {
              this.setState({ productData: response.data });
              message.error(`No ${category}`);
            })
            .catch(function(error) {
              console.log(`Error: ${error}`);
            });
        }
        this.setState({ productData: response.data });
      })
      .catch(error => {
        console.log(error);
        message.error(error.message);
      });
  };
  componentDidMount() {
    axios
      .get(`http://localhost:3001/product/category/all`)
      .then(response => {
        this.setState({ productData: response.data });
      })
      .catch(function(error) {
        console.log(`Error: ${error}`);
      });
  }
  render() {
    let images = this.state.productData.map(product => {
      return (
        <Link to={`/item/${this.state.userData.uid}`}>
          <div class="productGridCard">
            <div class="productGridImg">
              <img
                key={1}
                src={require(`../../assets/1.jpg`)}
                alt="Pei Ni Wan"
              />
            </div>
            <div class="productGridContent">
              <h2>RM{product.price}</h2>
              <h2>{product.service}</h2>
            </div>
          </div>
        </Link>
      );
    });

    let categories = GameCategories.map(games => {
      return (
        <div class="productGridCard">
          <div
            onClick={() => {
              this.selectCategory(games.Game);
            }}
          >
            <h2>{games.Game}</h2>
          </div>
        </div>
      );
    });

    return (
      <div>
        <div class="productGrid">{categories}</div>
        <div class="productGrid">{images}</div>
      </div>
    );
  }
}

export default ProductGrid;
