import React, { Component } from "react";
import "../../constants/css/Product.css";

let array = ["1", "2", "3", "4", "5"];

class ProductImg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainImg: array[0]
    };
  }

  render() {
    let images = array.map(image => {
      return (
        <img
          key={image}
          src={require(`../../assets/${image}.jpg`)}
          alt="Pei Ni Wan"
          onMouseOver={() => {
            this.setState({
              mainImg: image
            });
          }}
        />
      );
    });

    return (
      <div class="productDetailImage">
        <img
          class="productImg"
          key={this.state.mainImg}
          src={require(`../../assets/${this.state.mainImg}.jpg`)}
          alt="Pei Ni Wan"
        />

        <div class="productImgRow">{images}</div>
      </div>
    );
  }
}

export default ProductImg;
