import React, { Component } from "react";
import "../../../constants/css/Layout.css";
import HeaderBrand from "./HeaderBrand";
import HeaderNav from "./HeaderNav";
class Header extends Component {
  render() {
    return (
      <div class="header">
        <HeaderBrand title="Socius" />

        <HeaderNav />
      </div>
    );
  }
}

export default Header;
