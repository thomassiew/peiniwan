import React, { Component } from "react";
import "../../../constants/css/Layout.css";
import { Link } from "react-router-dom";

class HeaderBrand extends Component {
  render() {
    const { title } = this.props;
    return (
      <Link to="/" style={{textDecoration:"none"}}>
        <h1>{title}</h1>
      </Link>
    );
  }
}

export default HeaderBrand;
