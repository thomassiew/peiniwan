import React, { Component } from "react";
import "../../../constants/css/Layout.css";
import { Modal, Dropdown, Menu, Icon } from "antd";
import Login from "../../Auth/Login";
import { signOut } from "../../../config/firebase";
import { connect } from "react-redux";
import { openAuthModal, closeAuthModal } from "../../../redux/action/Auth";
import { Link } from "react-router-dom";

class HeaderNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null || JSON.parse(localStorage.getItem("user"))
    };
  }
  showModal = () => {
    this.props.dispatch(openAuthModal());
  };

  closeModal = () => {
    this.props.dispatch(closeAuthModal());
  };

  render() {
    let menu = (
      <Menu>
        <Menu.Item>
          <Link to="/profile">
            <a>Profile</a>
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/mail">
            <a>Mail</a>
          </Link>
        </Menu.Item>
        <Menu.Item>
          <div onClick={signOut}>
            <a>Sign Out</a>
          </div>
        </Menu.Item>
      </Menu>
    );
    return (
      <div class="headerNav">
        {this.state.user !== null ? (
          <Dropdown class="headerProfile" overlay={menu}>
            <h3>
              {this.state.user.displayName} <Icon type="down" />
            </h3>
          </Dropdown>
        ) : (
          <div onClick={this.showModal}>
            <h3>Login | Register</h3>
          </div>
        )}
        <Modal
          title="Login"
          visible={this.props.isOpenModal}
          footer={null}
          onCancel={this.closeModal}
          width={300}
        >
          <Login />
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isOpenModal: state.auth.isOpenModal
  };
};

export default connect(mapStateToProps)(HeaderNav);
