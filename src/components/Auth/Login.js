import React, { Component } from "react";
import FB from "../../assets/png/FB.png";
import Google from "../../assets/png/Google.png";
import { signInWithFacebook, signInWithGoogle } from "../../config/firebase";

class Login extends Component {
  
  render() {
    return (
      <div class="login">
        <h3> Sign-In With</h3>
        <img onClick={signInWithFacebook} src={FB} width="80" height="80" />

        <img onClick={signInWithGoogle} src={Google} width="80" height="80" />
      </div>
    );
  }
}

export default Login;
