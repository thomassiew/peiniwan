import React, { Component } from "react";
import { Route } from "react-router-dom";
import Product from "../pages/Product/Product";
import ProductDetail from "../pages/Product/ProductDetail";
import Profile from "../pages/Profile/Profile";
import MailBox from "../pages/Mail/MailBox";
class Routing extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={Product} />
        <Route path="/item/:uid" component={ProductDetail} />
        <Route path="/profile" component={Profile} />
        <Route path="/mail" component={MailBox} />
      </div>
    );
  }
}

export default Routing;
