import React, { Component } from "react";
import Header from "./components/Layout/Header/Header";
import "./constants/css/App.css";
import Footer from "./components/Layout/Footer/Footer";
import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./router/Routing";
import store from "./redux/store/index";
import { Provider } from "react-redux";

class App extends Component {
  constructor() {
    super();

    this.store = store();
  }

  render() {
    return (
      <Provider store={this.store}>
        <Router>
          <div className="App">
            <Header />
            <Routing />
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
