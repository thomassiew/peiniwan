export default {
  // Auth related
  OPEN_AUTH_MODAL : 'OPEN_AUTH_MODAL',
  CLOSE_AUTH_MODAL : 'CLOSE_AUTH_MODAL',
};
